import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import MovieDetail from './components/movies/MovieDetail';
import SignIn from './components/auth/SignIn';
import SignUp from './components/auth/SignUp';
import AddMovie from './components/movies/CreateMovie';
import AdminPage from './components/adminPage/Admin';
import UserList from './components/userPage/UserList';
import UserDetail from './components/userPage/UserDetail';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path='/' component={Dashboard} />
            <Route path='/sort/:type/:id' component={Dashboard} />
            <Route path='/movie/:id' component={MovieDetail} />
            <Route path='/signin' component={SignIn} />
            <Route path='/signup' component={SignUp} />
            <Route path='/add-movie' component={AddMovie} />
            <Route path='/admin-page' component={AdminPage} />
            <Route path='/user-list' component={UserList} />
            <Route path='/user/:id' component={UserDetail} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;
