import React, { Component } from 'react'
import UserSummary from './UserSummary'
import API from '../api/api';

class UserList extends Component {
  state = {
    users: [],
  }

  componentDidMount() {
    API.get(`/users`)
      .then(res => {
        const users = res.data.data;
        this.setState({ users })
      })
      .catch(err => console.log(err))
  }

  render() {
    return (
      <div className="user-list section">
        <UserSummary users={this.state.users} />
      </div>
    )
  }
}

export default UserList
