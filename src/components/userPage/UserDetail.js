import React, { Component } from 'react'
import API from '../api/api';
import UserFav from './UserFavMovie'

class UserDetail extends Component {
  state = {
    user:[],
  }
  componentDidMount() {
    API.get(`users/${this.props.match.params.id}`)
      .then(res => {
        const user = res.data.data[0];
        this.setState({ user })
      })
      .catch(err => console.log(err))
  }
  render() {
    const { username, email, is_admin } = this.state.user;

    return (
      <div className="container section user-details">
        <div className="card z-depth-0">
          <div className="card-content">
            <span className="card-title">{ username }</span>
          </div>
          <div className="card-action grey lighten-4 grey-text">
            <div>{email}</div>
            <div>account type : {is_admin ? 'admin' : 'user'}</div>
          </div>
        </div>
        <UserFav />
      </div>
    )
  }
}

export default UserDetail
