import React from 'react'
import { Link } from "react-router-dom";


const UserSummary = ({ users }) => {

  const userList = users.map(user => {
    return (
      <div className="card z-depth-0 user-summary" key={user.id}>
        <Link to={`/user/${user.id}`}>
          <div className="card-content grey-text text-darken-3">
            <span className="card-title ">{user.username}</span>
          </div>
          <div className="card-action grey lighten-4 grey-text">
            <p className="grey-text">{user.email}</p>
            <p className="grey-text">{user.is_admin ? 'admin' : 'user'}</p>
          </div>
        </Link>
      </div>
    )
  })
  return (
    <div className="user-list container">
      {userList}
    </div>
  )
}

export default UserSummary
