import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import MovieSummary from './MovieSummary'
import API from '../api/api';

class MovieList extends Component {
  state = {
    movies: [],
  }

  componentDidMount() {
    let API_path; 

    if(this.props.match.params.type === 'popular'){
      API_path = 'movies/popular'
    } else if(this.props.match.params.type === 'genre'){
      API_path = `movies/genre/${this.props.match.params.id}`
    }else{
      API_path = 'movies'
    }

    API.get(`${API_path}`)
      .then(res => {
        const movies = res.data.data;
        this.setState({movies})
      })
      .catch(err => console.log(err))
  }

  render() {
    return (
      <div className="movie-list section">  
        <MovieSummary movies={this.state.movies} />
      </div>
    )
  }
}

export default withRouter(MovieList)
