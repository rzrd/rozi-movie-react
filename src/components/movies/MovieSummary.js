import React from 'react'
import { Link } from 'react-router-dom';


const MovieSummary = ({ movies }) => {

  const movieList = movies.map(movie => {
    var d = new Date(movie.last_update)

    return (
      <div className="card z-depth-0 movie-summary" key={movie.id}>
        <div className="card-image">
          <img src={movie.cover} alt='movie poster'></img>
          <span className="card-title ">{movie.title}</span>
        </div>
        <div className="card-content grey-text text-darken-3">
          <div className="grey-text">genre: {movie.genre_name}</div>
          <div className="grey-text">status: {movie.status_name}</div>
          <div className="grey-text">posted: {d.toDateString()}</div>
          <div className="grey-text">views: {movie.views}</div>
        </div>
        <div className="card-action grey lighten-4 grey-text">
          <Link to={`/movie/${movie.id}`}>See Movie Details</Link>
        </div>
      </div>
    )
  })
  return (
    <div className="movie-list">
      {movieList}
    </div>
  )
}

export default MovieSummary
