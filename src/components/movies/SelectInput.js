import React from 'react'
import Select from 'react-select'

const SelectInput = ({ val, name, handleChange, options }) => {
  const names = `select ${name}`
  return (
    <div className="input-field">
      <Select
        id={name}
        isMulti={false}
        value={val}
        placeholder={names}
        onChange={handleChange}
        options={options}
      />
    </div>
  )
}

export default SelectInput
