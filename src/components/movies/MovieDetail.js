import React, { Component } from 'react'
import API from '../api/api';

class MovieDetail extends Component {
  state = {
    movie: [],
  }

  componentDidMount() {
    API.get(`movies/${this.props.match.params.id}`)
      .then(res => {
        const movie = res.data.data[0];
        this.setState({ movie })
      })
      .catch(err => console.log(err))
  }

  deleteMovie = () => {
    API.delete(`movies/${this.props.match.params.id}`,{
      headers: {
        'Content-Type': 'application/json', 'token': `${localStorage.getItem('token')}`
      }
    }).then(res => {
      alert(`success delete movie ${this.state.movie.title}`);
    }).catch(err => {
      alert(err)
    })
  }

  addLike = () => {
    API.put(`movies/like/${this.props.match.params.id}`, {}, {
      headers: {
        'Content-Type': 'application/json', 'token': `${localStorage.getItem('token')}`
      }
    }).then(res => {
      alert(`success favorite movie ${this.state.movie.title}`);
    }).catch(err => {
      alert(err)
    })
  }

  render() {
    const { title, cover, cast_name, producer_name, country_name, genre_name, status_name, last_update, views } = this.state.movie;
    const d = new Date(last_update);

    return (
      <div className="container section movie-details">
        <div className="card medium horizontal z-depth-0">
          <div className="card-image">
            <img src={cover} alt='movie poster'></img>
          </div>
          <div className="card-stacked">
            <div className="card-content grey-text text-darken-3">
              <h2 className="header">{title}</h2>
              <div>cast : {cast_name}</div>
              <div>producer : {producer_name}</div>
              <div>country : {country_name}</div>
              <button className="btn waves-effect waves-light red right" onClick={this.addLike}>
                Add Favorite
                <i className="material-icons right">favorite</i>
              </button>
            </div>
            <div className="card-action grey lighten-4 grey-text">
              <div className="grey-text">genre: {genre_name}</div>
              <div className="grey-text">status: {status_name}</div>
              <div className="grey-text">posted: {d.toDateString()}</div>
              <div className="grey-text">views: {views}</div>
              <div className='row s12'>
              <button className="col btn waves-effect waves-light green left" >
                Edit
                <i className="material-icons right">edit</i>
              </button>
              <button className="col btn waves-effect waves-light black right" onClick={this.deleteMovie}>
                Delete
                <i className="material-icons right">delete</i>
              </button>              
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MovieDetail
