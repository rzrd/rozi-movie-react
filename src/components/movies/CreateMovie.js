import React, { Component } from 'react';
import API from '../api/api';
import axios from 'axios';
import SelectInput from './SelectInput'

class CreateMovie extends Component {
  state = {
    title: '',
    cover: '',
    castId: '',
    producerId: '',
    countryId: '',
    genreId: '',
    statusId: '',
    castList: [],
    producerList: [],
    genreList: [],
    countryList: [],
    statusList: [],
    selectedOption1: null,
    selectedOption2: null,
    selectedOption3: null,
    selectedOption4: null,
    selectedOption5: null,
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleChange1 = selectedOption1 => {
    this.setState({ selectedOption1 });
    this.setState({ castId: selectedOption1.value });
  };

  handleChange2 = selectedOption2 => {
    this.setState({ selectedOption2 });
    this.setState({ producerId: selectedOption2.value });
  };

  handleChange3 = selectedOption3 => {
    this.setState({ selectedOption3 });
    this.setState({ genreId: selectedOption3.value });
  };

  handleChange4 = selectedOption4 => {
    this.setState({ selectedOption4 });
    this.setState({ countryId: selectedOption4.value });
  };

  handleChange5 = selectedOption5 => {
    this.setState({ selectedOption5 });
    this.setState({ statusId: selectedOption5.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
    const { title, cover, castId, producerId, genreId, countryId, statusId } = this.state;
    const data = {
      title: title,
      cover: cover,
      cast_id: castId,
      producer_id: producerId,
      genre_id: genreId,
      country_id: countryId,
      status_id: statusId,
    }

    API.post(`movies`, data, {
      headers: {
        'Content-Type': 'application/json', 'token': `${localStorage.getItem('token')}`
      }
    }).then(res => {
      alert(`success create movie ${title}`);
    }).then(() => {
      this.setState({
        title: '',
        cover: '',
        castId: '',
        producerId: '',
        countryId: '',
        genreId: '',
        statusId: '',
      })
      window.location.reload();
    }).catch(err => {
      alert(err)
    })
  }

  componentDidMount() {
    const getCast = API.get('cast');
    const getProducer = API.get('producer');
    const getGenre = API.get('genre');
    const getCountry = API.get('country');
    const getStatus = API.get('status');

    axios.all([getCast, getCountry, getGenre, getProducer, getStatus]).then(axios.spread((...res) => {
      const listCast = res[0].data.data
      const listCountry = res[1].data.data
      const listGenre = res[2].data.data
      const listProducer = res[3].data.data
      const listStatus = res[4].data.data

      this.setState(state => ({
        castList: [...state.castList, ...listCast],
        producerList: [...state.producerList, ...listProducer],
        genreList: [...state.genreList, ...listGenre],
        countryList: [...state.countryList, ...listCountry],
        statusList: [...state.statusList, ...listStatus],
      }))
    })).catch(error => {
      alert(error)
    })
  }

  render() {
    const isAdmin = localStorage.getItem('isAdmin')

    const options1 = this.state.castList.map(cast => {
      return { value: cast.id, label: cast.cast_name }
    })
    const options2 = this.state.producerList.map(producer => {
      return { value: producer.id, label: producer.producer_name }
    })
    const options3 = this.state.genreList.map(genre => {
      return { value: genre.id, label: genre.genre_name }
    })
    const options4 = this.state.countryList.map(country => {
      return { value: country.id, label: country.country_name }
    })
    const options5 = this.state.statusList.map(status => {
      return { value: status.id, label: status.status_name }
    })

    const { title, cover, selectedOption } = this.state

    return (
      <div className="container">
        {isAdmin === 'false' || isAdmin === null ? (
          <h2>You are not an Admin!</h2>
        ) : (
            <form className="white" onSubmit={this.handleSubmit} >
              <h5 className="grey-text text-darken-3">Create a New Movie</h5>
              <div className="input-field">
                <input type="text" id='title' value={title} required={true} onChange={this.handleChange} />
                <label htmlFor="title">Movie Title</label>
              </div>
              <div className="input-field">
                <input type="text" id='cover' value={cover} required={true} onChange={this.handleChange} />
                <label htmlFor="cover">Cover Image Link</label>
              </div>
              <SelectInput val={selectedOption} name='cast' handleChange={this.handleChange1} options={options1} />
              <SelectInput val={selectedOption} name='producer' handleChange={this.handleChange2} options={options2} />
              <SelectInput val={selectedOption} name='genre' handleChange={this.handleChange3} options={options3} />
              <SelectInput val={selectedOption} name='country' handleChange={this.handleChange4} options={options4} />
              <SelectInput val={selectedOption} name='status' handleChange={this.handleChange5} options={options5} />
              <div className="input-field">
                <button className="btn pink lighten-1">Create</button>
              </div>
            </form>
          )}
      </div>
    )
  }
}

export default CreateMovie
