import React from 'react'
import { NavLink } from 'react-router-dom'

const SignedInLinks = ({ logout }) => {
  const id = localStorage.getItem('user')

  return (
    <div>
      <ul className="right">
        <li><NavLink to='/user-list'>User List</NavLink></li>
        <li><NavLink to='/' onClick={logout}>Log Out</NavLink></li>
        <li><NavLink to={`/user/${id}`}><i className="material-icons">account_circle</i></NavLink></li>
      </ul>
    </div>
  )
}

export default SignedInLinks
