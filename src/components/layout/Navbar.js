import React, { Component } from 'react'
import SignedInLinks from './SignedInLinks'
import SignedOutLinks from './SignedOutLink'
import AdminLinks from './AdminLinks'


class Navbar extends Component {

  logout() {
    localStorage.removeItem("isLoggedIn");
    localStorage.removeItem("isAdmin");
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    window.location.reload();
  }

  render() {
    const isLoggedIn = localStorage.getItem('isLoggedIn')
    const isAdmin = localStorage.getItem('isAdmin')

    let navbarLink
    if (isLoggedIn && isAdmin === 'false') {
      navbarLink = <SignedInLinks logout={this.logout} />
    } else if (isAdmin) {
      navbarLink = <AdminLinks logout={this.logout} />
    } else {
      navbarLink = <SignedOutLinks />
    }

    return (
      <nav className="nav-wrapper grey darken-3">
        <div className="container">
          <a href='/' className="brand-logo">Gandool</a>
          {navbarLink}
        </div>
      </nav>
    )
  }
}

export default Navbar
