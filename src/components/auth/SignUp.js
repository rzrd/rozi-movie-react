import React, { Component } from 'react'
import API from '../api/api';

class SignUp extends Component {
  state = {
    email: '',
    password: '',
    username: '',
    account: 'user'
  }
  handleSelect= (e) => {
    this.setState({ account: e.target.value });
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password, username } = this.state
    const user = {
      email: email,
      password: password,
      username: username,
    };

    let path;

    if(this.state.account === 'admin'){path = 'admin'} else {path ='auth'};
    console.log(path)
    API.post(`${path}/signup`, user)
      .then(res => {
        alert('success create account, klik OK to login')
      })
      .then(() => {
        this.props.history.push('/signin');
      })
      .catch(err => {
        alert(err)
      })
  }
  render() {
    console.log(this.state)
    return (
      <div className="container">
        <form className="white" onSubmit={this.handleSubmit} >
          <h5 className="grey-text text-darken-3">Sign Up</h5>
          <div className="input-field">
            <label htmlFor="username">Username</label>
            <input type="text" id='username' required={true} onChange={this.handleChange}/>
          </div>
          <div className="input-field">
            <label htmlFor="email" >Email</label>
            <input type="email" id='email' className="validate" required={true} onChange={this.handleChange}/>
            <span className="helper-text" data-error="wrong format" data-success="right"></span>
          </div>
          <div className="input-field">
            <label htmlFor="password" >Password</label>
            <input type="password" id='password' required={true} onChange={this.handleChange}/>
          </div>
          <div className="input-field col s12">
            <select className="browser-default" onChange={this.handleSelect} required={true}>
              <option value="user">user</option>
              <option value="admin">admin</option>
            </select>
          </div>
          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Sign Up</button>
          </div>
        </form>
      </div>
    )
  }
}

export default SignUp
