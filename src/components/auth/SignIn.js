import React, { Component } from 'react'
import API from '../api/api';

class SignIn extends Component {
  state = {
    email: '',
    password: '',
    redirect:false,
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = this.state
    const user = {
      email: email,
      password: password,
    };

    API.post(`auth/signin`, user)
      .then(res => {
        const { is_admin, token, id } = res.data.data
        if (res.data.data) {
          localStorage.setItem("isLoggedIn", true);
          localStorage.setItem("isAdmin", is_admin);
          localStorage.setItem("token", token);
          localStorage.setItem("user", id);
          this.props.history.push("/");
        } else {
          this.props.history.push("/signin");
        }
      })
      .then(() => {
        window.location.reload();
      });
  }
  render() {
    return (
      <div className="container">
        <form className="white" onSubmit={this.handleSubmit} onChange={this.handleChange}>
          <h5 className="grey-text text-darken-3">Sign In</h5>
          <div className="input-field">
            <label htmlFor="email" >Email</label>
            <input type="email" id='email' className="validate" required={true} />
            <span className="helper-text" data-error="wrong format" data-success="right"></span>
          </div>
          <div className="input-field">
            <label htmlFor="password" >Password</label>
            <input type="password" id='password' required={true} />
          </div>
          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Login</button>
          </div>
        </form>
      </div>
    )
  }
}

export default SignIn
