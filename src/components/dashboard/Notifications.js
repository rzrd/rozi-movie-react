import React, { Component } from 'react'
import API from '../api/api';

class Notifications extends Component {
  state = {
    genres: [],
  }

  componentDidMount() {
    API.get(`genre`)
      .then(res => {
        const genres = res.data.data;
        this.setState({ genres })
      })
      .catch(err => console.log(err))
  }

  render() {
    const genreList = this.state.genres.map(genre => {
      return (
        <a href={`/sort/genre/${genre.id}`} key={genre.id}>{genre.genre_name}</a>
      )
    })

    return (
      <div className='section'>
        <div className="card z-depth-0 movie-summary" >
          <div className="card-action grey lighten-4 grey-text">
            <div className="input-field row">
              <input type="text" id='search' required={true} />
              <label htmlFor="title">Search Movie Title</label>
              <button className="btn waves-effect waves-light red right" >Search
                <i className="material-icons right">search</i>
              </button>
            </div>
          </div>
          <div className="card-action grey lighten-4 grey-text">
            <a href='/sort/popular/1'>Sort By Popular</a>
          </div>
          <div className="card-action grey lighten-4 grey-text">
            <div>Sort By Genre :</div>
            {genreList}
          </div>
        </div>
      </div>
    )
  }
}

export default Notifications
