import React, { Component } from 'react';
import API from '../api/api';
import AdminForm from './AdminForm';


class AdminPage extends Component {
  state = {
    cast: '',
    producer: '',
    country: '',
    genre: '',
    status: '',
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const bodyName = e.target.id + '_name'
    const objName = e.target.id

    API.post(`${objName}`, { [bodyName]: this.state[objName] }, {
      headers: {
        'Content-Type': 'application/json', 'token': `${localStorage.getItem('token')}`
      }
    }).then(res => {
      alert(`success create ${[objName]}`);
    }).catch(err => {
      alert(err)
    })

    this.setState({
      [objName]: ''
    })
  }

  render() {
    const isAdmin = localStorage.getItem('isAdmin')
    const { handleChange, handleSubmit, state } = this;

    return (
      <div className='container' >
        {isAdmin === 'false' || isAdmin === null ? (
          <h2>You are not an Admin!</h2>
        ) : (
            <div>
              <AdminForm name='cast' content={state} handleChange={handleChange} handleSubmit={handleSubmit} />
              <AdminForm name='producer' content={state} handleChange={handleChange} handleSubmit={handleSubmit} />
              <AdminForm name='country' content={state} handleChange={handleChange} handleSubmit={handleSubmit} />
              <AdminForm name='genre' content={state} handleChange={handleChange} handleSubmit={handleSubmit} />
              <AdminForm name='status' content={state} handleChange={handleChange} handleSubmit={handleSubmit} />
            </div>
          )}
      </div>
    )
  }
}

export default AdminPage