import React from 'react'

const AdminForm = ({ name, handleChange, handleSubmit, content }) => {
  return (
    <form className="white" id={name} onSubmit={handleSubmit} >
      <h5 className="grey-text text-darken-3">{name}</h5>
      <div className="input-field">
        <input type="text" id={name} required={true} onChange={handleChange} value={content[name]} />
        <label htmlFor={name}>type {name} name</label>
      </div>
      <div className="input-field">
        <button className="btn pink lighten-1 z-depth-0">add {name}</button>
      </div>
    </form>
  )
}

export default AdminForm
